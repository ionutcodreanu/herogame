<?php

namespace HeroGame;

class PlayerBuilder
{
    /**
     * @var RangeNumberGenerator
     */
    private $rangeNumberGenerator;
    /** @var Player */
    private $player;

    public function __construct(RangeNumberGenerator $rangeNumberGenerator)
    {
        $this->rangeNumberGenerator = $rangeNumberGenerator;
        $this->player = new Player();
    }

    public function build(): Player
    {
        return $this->player;
    }

    /**
     * @param int $inferiorLimit
     * @param int $superiorLimit
     * @return PlayerBuilder
     * @throws Exceptions\PlayerInvalidHealth
     */
    public function withHealthRange(int $inferiorLimit, int $superiorLimit): PlayerBuilder
    {
        $health = $this->rangeNumberGenerator->numberBetween($inferiorLimit, $superiorLimit);
        $this->player->setHealth($health);
        return $this;
    }

    /**
     * @param int $inferiorLimit
     * @param int $superiorLimit
     * @return PlayerBuilder
     * @throws Exceptions\PlayerInvalidStrength
     */
    public function withStrengthRange(int $inferiorLimit, int $superiorLimit): PlayerBuilder
    {
        $strength = $this->rangeNumberGenerator->numberBetween($inferiorLimit, $superiorLimit);
        $this->player->setStrength($strength);
        return $this;
    }

    /**
     * @param int $inferiorLimit
     * @param int $superiorLimit
     * @return PlayerBuilder
     * @throws Exceptions\PlayerInvalidDefense
     */
    public function withDefenseRange(int $inferiorLimit, int $superiorLimit): PlayerBuilder
    {
        $defense = $this->rangeNumberGenerator->numberBetween($inferiorLimit, $superiorLimit);
        $this->player->setDefense($defense);
        return $this;
    }

    /**
     * @param int $inferiorLimit
     * @param int $superiorLimit
     * @return PlayerBuilder
     * @throws Exceptions\PlayerInvalidSpeed
     */
    public function withSpeedRange(int $inferiorLimit, int $superiorLimit): PlayerBuilder
    {
        $speed = $this->rangeNumberGenerator->numberBetween($inferiorLimit, $superiorLimit);
        $this->player->setSpeed($speed);
        return $this;
    }

    /**
     * @param int $inferiorLimit
     * @param int $superiorLimit
     * @return PlayerBuilder
     * @throws Exceptions\PlayerInvalidLuck
     */
    public function withLuckRange(int $inferiorLimit, int $superiorLimit): PlayerBuilder
    {
        $luck = $this->rangeNumberGenerator->numberBetween($inferiorLimit, $superiorLimit);
        $this->player->setLuck($luck);
        return $this;
    }
}
