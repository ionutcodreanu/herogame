<?php

namespace HeroGame;

use HeroGame\Exceptions\PlayerInvalidDefense;
use HeroGame\Exceptions\PlayerInvalidHealth;
use HeroGame\Exceptions\PlayerInvalidLuck;
use HeroGame\Exceptions\PlayerInvalidSpeed;
use HeroGame\Exceptions\PlayerInvalidStrength;
use HeroGame\Skills\Skill;
use HeroGame\Skills\SkillCollection;

class Player
{
    /** @var  int */
    private $health = 0;
    /** @var  int */
    private $defense = 0;
    /** @var  int */
    private $strength = 0;
    /** @var  int */
    private $speed = 0;
    /** @var  int */
    private $luck = 0;
    /** @var  SkillCollection */
    private $skills;
    /** @var  string */
    private $name = '';
    /** @var int */
    private $lastDamage = 0;

    public function __construct()
    {
        $this->skills = new SkillCollection();
    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Player
     */
    public function setName(string $name): Player
    {
        $this->name = $name;
        return $this;
    }

    public function addDamage(int $damage): void
    {
        $health = $this->getHealth() - $damage;
        $this->health = $health;
        $this->lastDamage = $damage;
    }

    /**
     * @return int
     */
    public function getHealth(): int
    {
        return $this->health;
    }

    /**
     * @param int $health
     * @throws PlayerInvalidHealth
     */
    public function setHealth(int $health)
    {
        if ($health <= 0 || $health > 100) {
            throw new PlayerInvalidHealth('Player health should be between 0 and 100');
        }
        $this->health = $health;
    }

    /**
     * @return int
     */
    public function getDefense(): int
    {
        return $this->defense;
    }

    /**
     * @param int $defense
     * @throws PlayerInvalidDefense
     */
    public function setDefense(int $defense)
    {
        if ($defense <= 0 || $defense > 100) {
            throw new PlayerInvalidDefense('Player defense should be between 0 and 100');
        }
        $this->defense = $defense;
    }

    /**
     * @return int
     */
    public function getStrength(): int
    {
        return $this->strength;
    }

    /**
     * @param int $strength
     * @throws PlayerInvalidStrength
     */
    public function setStrength(int $strength)
    {
        if ($strength <= 0 || $strength > 100) {
            throw new PlayerInvalidStrength('Player strength should be between 0 and 100');
        }
        $this->strength = $strength;
    }

    /**
     * @return int
     */
    public function getSpeed(): int
    {
        return $this->speed;
    }

    /**
     * @param int $speed
     * @throws PlayerInvalidSpeed
     */
    public function setSpeed(int $speed)
    {
        if ($speed <= 0 || $speed > 100) {
            throw new PlayerInvalidSpeed('Player speed should be between 0 and 100');
        }
        $this->speed = $speed;
    }

    /**
     * @return int
     */
    public function getLuck(): int
    {
        return $this->luck;
    }

    /**
     * @param int $luck
     * @throws PlayerInvalidLuck
     */
    public function setLuck(int $luck)
    {
        if ($luck < 0 || $luck > 100) {
            throw new PlayerInvalidLuck('Player luck should be between 0 and 100');
        }
        $this->luck = $luck;
    }

    public function addSkill(Skill $skill)
    {
        $this->skills->addSkill($skill);
    }

    /**
     * @param Player $attacker
     * @return Skill[]
     */
    public function defend(Player $attacker): array
    {
        $defender = $this;
        $powerfulDefensiveThanAttacker = $attacker->getStrength() <= $defender->getDefense();
        if ($powerfulDefensiveThanAttacker) {
            return [];
        }

        $damage = $attacker->getDamage($this);
        $defender->addDamage($damage);

        return $this->applyDefensiveSkills($attacker, $defender);
    }

    /**
     * @param Player $defender
     * @return Skill[]
     */
    public function attackWithoutSkills(Player $defender): array
    {
        $damage = $this->getDamage($defender);
        if ($damage === 0) {
            return [];
        }
        if ($damage < 0) {
            $this->addDamage(-1 * $damage);
        }

        return $defender->defend($this);
    }

    /**
     * @param Player $defender
     * @return int
     */
    public function getDamage(Player $defender): int
    {
        return $this->getStrength() - $defender->getDefense();
    }

    public function attack(Player $defender): array
    {
        $defensiveSkillsApplied = $this->attackWithoutSkills($defender);
        $offensiveSkillsApplied = $this->applySkillsForAttacking($defender);

        return [$offensiveSkillsApplied, $defensiveSkillsApplied];
    }

    /**
     * @param Player $defender
     * @return Skill[]
     */
    private function applySkillsForAttacking(Player $defender): array
    {
        $offensiveSkills = $this->getOffensiveSkills();
        $attacker = $this;
        return $this->applySkills($attacker, $defender, $offensiveSkills);
    }

    /**
     * @param Player $attacker
     * @param Player $defender
     * @return Skill[]
     */
    private function applyDefensiveSkills(Player $attacker, Player $defender): array
    {
        $defensiveSkills = $this->getDefensiveSkills();
        return $this->applySkills($attacker, $defender, $defensiveSkills);
    }

    /**
     * @return Skill[]
     */
    private function getOffensiveSkills(): array
    {
        return $this->skills->getOffensiveSkills();
    }

    /**
     * @return Skill[]
     */
    private function getDefensiveSkills(): array
    {
        return $this->skills->getDefensiveSkills();
    }

    /**
     * @param Player $attacker
     * @param Player $defender
     * @param $skills
     * @return Skill[]
     */
    private function applySkills(Player $attacker, Player $defender, $skills): array
    {
        $skillsApplied = [];
        foreach ($skills as $skill) {
            /** @var Skill $skill */
            $applied = $skill->apply($attacker, $defender);
            if ($applied) {
                $skillsApplied[] = $skill;
            }
        }

        return $skills;
    }

    public function getLastDamage(): int
    {
        return $this->lastDamage;
    }
}
