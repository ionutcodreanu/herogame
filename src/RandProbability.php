<?php

namespace HeroGame;

/**
 * Naive implementation, more research should be made for probability generator
 *
 * Class RandProbability
 * @package HeroGame
 */
class RandProbability implements LuckProbability
{
    use ProbabilityGenerator;

    /**
     * @param int $probability
     * @return bool
     * @
     */
    public function iAmLucky(int $probability): bool
    {
        return $this->generate($probability);
    }
}
