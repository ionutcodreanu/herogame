<?php

namespace HeroGame;

interface LuckProbability
{
    public function iAmLucky(int $probability): bool;
}
