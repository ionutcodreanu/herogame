<?php

namespace HeroGame;

class PlayerFactory
{
    /**
     * @param int $health
     * @param int $strength
     * @param int $defense
     * @param int $speed
     * @param int $luck
     * @return Player
     * @throws Exceptions\PlayerInvalidDefense
     * @throws Exceptions\PlayerInvalidHealth
     * @throws Exceptions\PlayerInvalidLuck
     * @throws Exceptions\PlayerInvalidSpeed
     * @throws Exceptions\PlayerInvalidStrength
     */
    public function createPlayer(int $health, int $strength, int $defense, int $speed, int $luck)
    {
        $player = new Player();
        $player->setHealth($health);
        $player->setStrength($strength);
        $player->setDefense($defense);
        $player->setSpeed($speed);
        $player->setLuck($luck);

        return $player;
    }
}
