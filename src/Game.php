<?php

namespace HeroGame;

use HeroGame\Exceptions\GameNumberOfPlayers;
use HeroGame\Game\Round\Result;

class Game
{
    public const MAX_NUMBER_OF_ROUNDS = 20;
    /** @var  Player[] */
    private $players;
    /**
     * @var LuckProbability
     */
    private $luckProbability;

    /** @var \SplObjectStorage */
    private $observers;

    public function __construct(LuckProbability $luckProbability)
    {
        $this->luckProbability = $luckProbability;
        $this->observers = new \SplObjectStorage();
    }

    /**
     * @return GameResult
     * @throws GameNumberOfPlayers
     */
    public function play(): GameResult
    {
        $this->validateNumberOfPlayers();
        $gameResult = new GameResult();
        list($firstRoundAttacker, $firstRoundDefender) = $this->getFirstRoundOrder();
        $gameResult->setFirstRoundAttacker($firstRoundAttacker);

        $attacker = $firstRoundAttacker;
        $defender = $firstRoundDefender;
        $currentRound = 0;
        while ($currentRound < self::MAX_NUMBER_OF_ROUNDS) {
            $currentRound++;
            $roundResult = $this->playRound($attacker, $defender, $currentRound);
            $gameResult->addRoundResult($roundResult);
            $gameEnded = $attacker->getHealth() <= 0 || $defender->getHealth() <= 0;
            if ($gameEnded) {
                break;
            }
            list($attacker, $defender) = $this->switchAttackerWithDefender($attacker, $defender);
        }

        $gameResult->setNumberOfRounds($currentRound);

        if (self::MAX_NUMBER_OF_ROUNDS === $currentRound) {
            $gameResult->setWinnerPlayer(new NoWinner());
        } else {
            $winner = $this->getWinner($attacker, $defender);
            $gameResult->setWinnerPlayer($winner);
        }
        return $gameResult;
    }

    /**
     * @param Player $player
     * @throws GameNumberOfPlayers
     */
    public function addPlayer(Player $player): void
    {
        if ($this->getNumberOfPlayers() == 2) {
            throw new GameNumberOfPlayers('The maximum number of players is 2');
        }
        $this->players[] = $player;
    }

    /**
     * @return int
     */
    private function getNumberOfPlayers(): int
    {
        return count($this->players);
    }

    /**
     * @return Player[]
     */
    private function getFirstRoundOrder(): array
    {
        list($firstPlayer, $secondPlayer) = $this->players;

        if ($firstPlayer->getSpeed() === $secondPlayer->getSpeed()) {
            if ($firstPlayer->getLuck() > $secondPlayer->getLuck()) {
                return [$firstPlayer, $secondPlayer];
            } else {
                return [$secondPlayer, $firstPlayer];
            }
        }

        if ($firstPlayer->getSpeed() > $secondPlayer->getSpeed()) {
            return [$firstPlayer, $secondPlayer];
        } else {
            return [$secondPlayer, $firstPlayer];
        }
    }

    private function playRound(Player $attacker, Player $defender, int $roundNumber): Result
    {
        $defenderLucky = false;
        if (false === $this->defenderIsLucky($defender)) {
            $skills = $attacker->attack($defender);
        } else {
            $defenderLucky = true;
        }
        $result = $this->createRoundResult($roundNumber, clone $attacker, clone $defender);
        if ($defenderLucky) {
            $result->setDefenderWasLucky($defenderLucky);
        } else {
            $result->setAttackerSkills(array_shift($skills));
            $result->setDefenderSkills(array_shift($skills));
        }
        return $result;
    }

    /**
     * @param Player $attacker
     * @param Player $defender
     * @return Player[]
     */
    private function switchAttackerWithDefender(Player $attacker, Player $defender): array
    {
        return [$defender, $attacker];
    }

    private function getWinner(Player $player, Player $otherPlayer): Player
    {
        if ($player->getHealth() <= 0) {
            return $otherPlayer;
        } else {
            return $player;
        }
    }

    private function defenderIsLucky(Player $defender): bool
    {
        $iAmLucky = $this->luckProbability->iAmLucky($defender->getLuck());
        return $iAmLucky;
    }

    private function createRoundResult(int $currentRound, Player $attacker, Player $defender): Result
    {
        $roundResult = new Result($currentRound, $attacker, $defender);

        return $roundResult;
    }

    /**
     * @throws GameNumberOfPlayers
     */
    private function validateNumberOfPlayers(): void
    {
        $numberOfPlayers = $this->getNumberOfPlayers();
        if ($numberOfPlayers == 0) {
            throw new GameNumberOfPlayers('The game can not start without no players');
        } elseif ($numberOfPlayers == 1) {
            throw new GameNumberOfPlayers('The game can not start with only one player');
        }
    }
}
