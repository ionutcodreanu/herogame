<?php

namespace HeroGame\Skills;

use HeroGame\Player;

class DoubleStrike extends Skill
{
    const NAME = 'Double Strike';
    const TYPE = Skill::OFFENSIVE;

    protected function applySkill(Player $attacker, Player $defender): void
    {
        $attacker->attackWithoutSkills($defender);
    }
}
