<?php

namespace HeroGame\Skills;

use HeroGame\Player;

class TripleStrike extends Skill
{
    const NAME = 'Triple Strike';
    const TYPE = Skill::OFFENSIVE;

    protected function applySkill(Player $attacker, Player $defender): void
    {
        $attacker->attackWithoutSkills($defender);
        $attacker->attackWithoutSkills($defender);
    }
}
