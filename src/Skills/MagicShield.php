<?php

namespace HeroGame\Skills;

use HeroGame\Player;

class MagicShield extends Skill
{
    const NAME = 'Magic Shield';
    const TYPE = Skill::DEFENSIVE;

    protected function applySkill(Player $attacker, Player $defender): void
    {
        $damage = $attacker->getDamage($defender);
        $defender->addDamage(-1 * (int)($damage / 2));
    }
}
