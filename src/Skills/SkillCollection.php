<?php

namespace HeroGame\Skills;

class SkillCollection
{
    private $skills = [];

    public function addSkill(Skill $skill)
    {
        $this->skills[] = $skill;
    }

    /**
     * @return Skill[]
     */
    public function getOffensiveSkills(): array
    {
        return array_filter($this->skills, function (Skill $skill) {
            if ($skill->isOffensive()) {
                return true;
            }

            return false;
        });
    }

    /**
     * @return Skill[]
     */
    public function getDefensiveSkills(): array
    {
        $defensiveSkills = array_filter($this->skills, function (Skill $skill) {
            return $skill->getType() == Skill::DEFENSIVE;
        });
        return $defensiveSkills;
    }
}
