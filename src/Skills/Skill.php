<?php

namespace HeroGame\Skills;

use HeroGame\Player;

abstract class Skill
{
    const DEFENSIVE = 1;
    const OFFENSIVE = 2;
    const NAME = '';
    const TYPE = Skill::DEFENSIVE;

    /** @var Chance */
    private $chance = null;

    /**
     * @return int
     */
    public function getType(): int
    {
        return static::TYPE;
    }

    public function isOffensive()
    {
        return self::OFFENSIVE === static::TYPE;
    }

    /**
     * @param Player $attacker
     * @param Player $defender
     * @return bool if skill was applied
     */
    final public function apply(Player $attacker, Player $defender): bool
    {
        if (false === $this->shouldApply()) {
            return false;
        }

        $this->applySkill($attacker, $defender);
        return true;
    }

    public function withChance(Chance $chance)
    {
        $this->chance = $chance;
    }

    protected function shouldApply(): bool
    {
        if (null !== $this->chance && !$this->chance->iHaveChance()) {
            return false;
        }

        return true;
    }

    public function getName(): string
    {
        return static::NAME;
    }

    abstract protected function applySkill(Player $attacker, Player $defender): void;
}
