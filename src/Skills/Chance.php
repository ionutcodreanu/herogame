<?php

namespace HeroGame\Skills;

interface Chance
{
    public function iHaveChance();
}
