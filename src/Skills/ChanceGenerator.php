<?php

namespace HeroGame\Skills;

use HeroGame\ProbabilityGenerator;

class ChanceGenerator implements Chance
{
    use ProbabilityGenerator;
    /**
     * @var int
     */
    private $chance;

    public function __construct(int $chance)
    {
        $this->chance = $chance;
    }

    public function iHaveChance()
    {
        return $this->generate($this->chance);
    }
}
