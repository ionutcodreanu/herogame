<?php

namespace HeroGame;

class RandomRangeGenerator implements RangeNumberGenerator
{
    public function numberBetween(int $inferiorLimit, int $superiorLimit): int
    {
        try {
            $randomNumber = random_int($inferiorLimit, $superiorLimit);
        } catch (\Exception $e) {
            $randomNumber = mt_rand($inferiorLimit, $superiorLimit);
        }

        return (int)$randomNumber;
    }
}
