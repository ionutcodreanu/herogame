<?php

namespace HeroGame;

use HeroGame\Game\Round\Result;
use HeroGame\Game\Round\ResultCollection;

class GameResult
{
    /** @var  Player */
    private $firstRoundAttacker;
    /** @var int */
    private $numberOfRounds = 0;
    /** @var  Player */
    private $winnerPlayer;

    /** @var ResultCollection */
    private $resultsPerRound;

    public function __construct()
    {
        $this->resultsPerRound = new ResultCollection();
    }

    public function getNumberOfRounds(): int
    {
        return $this->numberOfRounds;
    }

    /**
     * @return Player
     */
    public function getFirstRoundAttacker(): Player
    {
        return $this->firstRoundAttacker;
    }

    /**
     * @param Player $firstRoundAttacker
     */
    public function setFirstRoundAttacker(Player $firstRoundAttacker): void
    {
        $this->firstRoundAttacker = $firstRoundAttacker;
    }

    /**
     * @param int $numberOfRounds
     */
    public function setNumberOfRounds(int $numberOfRounds): void
    {
        $this->numberOfRounds = $numberOfRounds;
    }

    /**
     * @return Player
     */
    public function getWinnerPlayer(): Player
    {
        return $this->winnerPlayer;
    }

    /**
     * @param Player $winnerPlayer
     */
    public function setWinnerPlayer(Player $winnerPlayer): void
    {
        $this->winnerPlayer = $winnerPlayer;
    }

    /**
     * @return ResultCollection
     */
    public function getRoundResult(): ResultCollection
    {
        return $this->resultsPerRound;
    }

    public function addRoundResult(Result $roundResult)
    {
        $this->resultsPerRound->attach($roundResult);
    }
}
