<?php

namespace HeroGame;

trait ProbabilityGenerator
{
    protected function generate(int $probability)
    {
        if ($probability === 100) {
            return true;
        }
        try {
            $randomNumber = random_int(0, 100);
        } catch (\Exception $exception) {
            $randomNumber = mt_rand(0, 100);
        }
        return $randomNumber <= $probability;
    }
}
