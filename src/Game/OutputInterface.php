<?php

namespace HeroGame\Game;

interface OutputInterface
{
    /**
     * @param string $message
     * @return void
     */
    public function writeln(string $message): void;
}
