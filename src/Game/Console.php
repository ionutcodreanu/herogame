<?php

namespace HeroGame\Game;

class Console implements OutputInterface
{
    public function writeln(string $message): void
    {
        echo $message . "\n";
    }
}
