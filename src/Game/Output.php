<?php

namespace HeroGame\Game;

use HeroGame\Game\Round\Result;
use HeroGame\GameResult;
use HeroGame\NoWinner;

class Output
{
    /**
     * @var OutputInterface
     */
    private $output;

    public function __construct(OutputInterface $output)
    {
        $this->output = $output;
    }

    public function generate(GameResult $gameResult): void
    {
        $this->printEveryRoundResult($gameResult);
        $winner = $gameResult->getWinnerPlayer();
        $this->output->writeln('Game Result:');
        if ($winner instanceof NoWinner) {
            $this->output->writeln("\tNo game winner.");
        } else {
            $this->output->writeln(
                sprintf("\t%s wins the game.", $winner->getName())
            );
        }
    }

    private function printEveryRoundResult(GameResult $gameResult): void
    {
        foreach ($gameResult->getRoundResult() as $roundResult) {
            /** @var Result $roundResult */
            $attacker = $roundResult->getAttacker();
            $defender = $roundResult->getDefender();
            $this->output->writeln(sprintf("Round %d:", $roundResult->getRoundNumber()));
            $this->output->writeln(
                sprintf("\t%s attacked %s.", $attacker->getName(), $defender->getName())
            );
            foreach ($roundResult->getAttackerSkills() as $skill) {
                $this->output->writeln(
                    sprintf("\t%s applied %s skill.", $attacker->getName(), $skill->getName())
                );
            }

            if ($roundResult->wasDefenderLucky()) {
                $this->output->writeln(
                    sprintf("\t%s was lucky and avoided attack.", $defender->getName())
                );
            } else {
                $this->output->writeln(
                    sprintf("\t%s damage was %d.", $defender->getName(), $defender->getLastDamage())
                );
                foreach ($roundResult->getDefensiveSkills() as $skill) {
                    $this->output->writeln(
                        sprintf("\t%s applied %s skill.", $defender->getName(), $skill->getName())
                    );
                }
            }
            $this->output->writeln(
                sprintf("\t%s current health is: %d", $defender->getName(), $defender->getHealth())
            );
        }
    }
}
