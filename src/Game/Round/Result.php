<?php

namespace HeroGame\Game\Round;

use HeroGame\Player;
use HeroGame\Skills\Skill;

class Result
{
    /**
     * @var int
     */
    private $roundNumber;
    /**
     * @var Player
     */
    private $attacker;
    /**
     * @var Player
     */
    private $defender;

    /** @var bool */
    private $luckyDefender = false;
    /** @var Skill[] */
    private $attackerSkills = [];
    /** @var Skill[] */
    private $defensiveSkills = [];

    public function __construct(int $roundNumber, Player $attacker, Player $defender)
    {
        $this->roundNumber = $roundNumber;
        $this->attacker = $attacker;
        $this->defender = $defender;
    }

    public function getRoundNumber(): int
    {
        return $this->roundNumber;
    }

    public function getAttacker(): Player
    {
        return $this->attacker;
    }

    public function getDefender(): Player
    {
        return $this->defender;
    }

    public function setDefenderWasLucky(bool $defenderLucky): void
    {
        $this->luckyDefender = $defenderLucky;
    }

    public function wasDefenderLucky(): bool
    {
        return $this->luckyDefender;
    }

    /**
     * @param Skill[] $offensiveSkills
     */
    public function setAttackerSkills(array $offensiveSkills): void
    {
        $this->attackerSkills = $offensiveSkills;
    }

    /**
     * @param Skill[] $defensiveSkills
     */
    public function setDefenderSkills(array $defensiveSkills): void
    {
        $this->defensiveSkills = $defensiveSkills;
    }

    /**
     * @return Skill[]
     */
    public function getAttackerSkills(): array
    {
        return $this->attackerSkills;
    }

    /**
     * @return Skill[]
     */
    public function getDefensiveSkills(): array
    {
        return $this->defensiveSkills;
    }
}
