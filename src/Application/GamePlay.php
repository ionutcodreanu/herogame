<?php

namespace HeroGame\Application;

use HeroGame\Game;
use HeroGame\Game\Output;
use HeroGame\Player;
use HeroGame\PlayerBuilder;
use HeroGame\RandProbability;
use HeroGame\RangeNumberGenerator;

class GamePlay
{
    /**
     * @var Output
     */
    private $output;
    /**
     * @var RangeNumberGenerator
     */
    private $rangeNumberGenerator;

    public function __construct(Output $output, RangeNumberGenerator $rangeNumberGenerator)
    {
        $this->output = $output;
        $this->rangeNumberGenerator = $rangeNumberGenerator;
    }

    public function play()
    {
        $heroStatsRange = [
            'health' => [70, 100],
            'strength' => [70, 80],
            'defense' => [45, 55],
            'speed' => [40, 50],
            'luck' => [10, 30],
        ];

        $wildBestStatsRange = [
            'health' => [60, 90],
            'strength' => [60, 90],
            'defense' => [40, 60],
            'speed' => [40, 60],
            'luck' => [25, 40],
        ];

        $orderus = $this->initPlayer($heroStatsRange);
        $orderus->setName('Orderus');
        $wildBeast = $this->initPlayer($wildBestStatsRange);
        $wildBeast->setName('WildBeast');

        $game = new Game(new RandProbability());
        $game->addPlayer($orderus);
        $game->addPlayer($wildBeast);

        $gameResult = $game->play();
        $this->output->generate($gameResult);
    }

    private function initPlayer($playerStatsRange): Player
    {
        $playerBuilder = new PlayerBuilder($this->rangeNumberGenerator);
        return $playerBuilder
            ->withHealthRange($playerStatsRange['health'][0], $playerStatsRange['health'][1])
            ->withStrengthRange($playerStatsRange['strength'][0], $playerStatsRange['strength'][1])
            ->withDefenseRange($playerStatsRange['defense'][0], $playerStatsRange['defense'][1])
            ->withSpeedRange($playerStatsRange['speed'][0], $playerStatsRange['speed'][1])
            ->withLuckRange($playerStatsRange['luck'][0], $playerStatsRange['luck'][1])
            ->build();
    }
}
