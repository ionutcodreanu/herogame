<?php

namespace HeroGame;

interface RangeNumberGenerator
{
    public function numberBetween(int $inferiorLimit, int $superiorLimit): int;
}
