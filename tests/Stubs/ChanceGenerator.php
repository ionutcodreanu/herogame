<?php

namespace HeroGame\Tests\Stubs;

use HeroGame\Skills\Chance;

class ChanceGenerator implements Chance
{
    /**
     * @var int
     */
    private $chance;

    public function __construct(int $chance)
    {
        $this->chance = $chance;
    }

    public function iHaveChance()
    {
        if ($this->chance === 100) {
            return true;
        }

        return false;
    }
}
