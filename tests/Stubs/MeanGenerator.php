<?php

namespace HeroGame\Tests\Stubs;

use HeroGame\RangeNumberGenerator;

class MeanGenerator implements RangeNumberGenerator
{
    public function numberBetween(int $inferiorLimit, int $superiorLimit): int
    {
        $mean = ($inferiorLimit + $superiorLimit) / 2;
        return ceil($mean);
    }
}
