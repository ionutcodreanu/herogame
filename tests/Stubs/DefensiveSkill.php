<?php

namespace HeroGame\Tests\Stubs;

class DefensiveSkill extends Skill
{
    const NAME = 'Defensive';
    const TYPE = \HeroGame\Skills\Skill::DEFENSIVE;
}
