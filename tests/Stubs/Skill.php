<?php

namespace HeroGame\Tests\Stubs;

use HeroGame\Player;

class Skill extends \HeroGame\Skills\Skill
{
    public $noOfOccurrences = 0;

    protected function applySkill(Player $attacker, Player $defender): void
    {
        $this->noOfOccurrences++;
    }
}
