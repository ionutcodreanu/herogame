<?php

namespace HeroGame\Tests\Stubs;

use HeroGame\LuckProbability;

class NeverLucky implements LuckProbability
{
    public function iAmLucky(int $probability): bool
    {
        return false;
    }
}
