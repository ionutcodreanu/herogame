<?php

namespace HeroGame\Tests\Stubs;

class OffensiveSkill extends Skill
{
    const NAME = 'Offensive';
    const TYPE = \HeroGame\Skills\Skill::OFFENSIVE;
}
