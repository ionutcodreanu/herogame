<?php

namespace HeroGame\Tests\Stubs;

use HeroGame\LuckProbability;

class ControlledLuck implements LuckProbability
{
    public function iAmLucky(int $probability): bool
    {
        if (100 === $probability) {
            return true;
        }
        return false;
    }
}
