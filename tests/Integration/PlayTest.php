<?php

namespace HeroGame\Tests\Integration;

use HeroGame\Game;
use HeroGame\LuckProbability;
use HeroGame\NoWinner;
use HeroGame\Player;
use HeroGame\PlayerFactory;
use HeroGame\Tests\Stubs\NeverLucky;
use PHPUnit\Framework\TestCase;

class PlayTest extends TestCase
{
    /**
     * @var PlayerFactory
     */
    private $playerFactory;
    /** @var  LuckProbability */
    private $luckProbability;

    public function gameScenariosWinnerFirstPlayer()
    {
        return [
            [
                [
                    'health' => 80,
                    'strength' => 70,
                    'defense' => 60,
                ],
                [
                    'health' => 80,
                    'strength' => 65,
                    'defense' => 50,
                ],
                65,
                0
            ],
            [
                [
                    'health' => 80,
                    'strength' => 70,
                    'defense' => 60,
                ],
                [
                    'health' => 79,
                    'strength' => 65,
                    'defense' => 50,
                ],
                65,
                -1
            ],
            [
                [
                    'health' => 80,
                    'strength' => 70,
                    'defense' => 60,
                ],
                [
                    'health' => 76,
                    'strength' => 65,
                    'defense' => 50,
                ],
                65,
                -4
            ],
            [
                [
                    'health' => 79,
                    'strength' => 70,
                    'defense' => 60,
                ],
                [
                    'health' => 76,
                    'strength' => 86,
                    'defense' => 50,
                ],
                1,
                -4
            ],
            [
                [
                    'health' => 80,
                    'strength' => 70,
                    'defense' => 60,
                ],
                [
                    'health' => 100,
                    'strength' => 65,
                    'defense' => 50,
                ],
                60,
                0,
                9
            ],
            [
                [
                    'health' => 80,
                    'strength' => 75,
                    'defense' => 60,
                ],
                [
                    'health' => 100,
                    'strength' => 65,
                    'defense' => 50,
                ],
                65,
                0,
                7
            ],
            [
                [
                    'health' => 80,
                    'strength' => 60,
                    'defense' => 60,
                ],
                [
                    'health' => 41,
                    'strength' => 65,
                    'defense' => 55,
                ],
                40,
                -4,
                17
            ],
            [
                [
                    'health' => 100,
                    'strength' => 100,
                    'defense' => 100,
                ],
                [
                    'health' => 40,
                    'strength' => 90,
                    'defense' => 90,
                ],
                100,
                0,
                4
            ],
        ];
    }

    protected function setUp()
    {
        $this->playerFactory = new PlayerFactory();
        $this->luckProbability = new NeverLucky();
    }

    /**
     * after twenty rounds there is no winner
     */
    public function testAfterTwentyRoundsThereIsNoWinner()
    {
        $attacker = $this->playerFactory->createPlayer(100, 80, 79, 80, 80);
        $defender = $this->playerFactory->createPlayer(100, 80, 79, 80, 80);

        $gameResult = $this->playGame($attacker, $defender);
        static::assertEquals(20, $gameResult->getNumberOfRounds());
        static::assertInstanceOf(NoWinner::class, $gameResult->getWinnerPlayer());
    }


    /**
     * first player win after eight rounds
     * @dataProvider gameScenariosWinnerFirstPlayer
     * @param $winnerProperties
     * @param $loserProperties
     * @param $winnerHealth
     * @param $loserHealth
     * @param int $rounds
     * @throws \HeroGame\Exceptions\GameNumberOfPlayers
     */
    public function testFirstPlayerWinAfterXNumberOfRounds(
        $winnerProperties,
        $loserProperties,
        $winnerHealth,
        $loserHealth,
        $rounds = 7
    ) {
        $winner = $this->playerFactory->createPlayer(
            $winnerProperties['health'],
            $winnerProperties['strength'],
            $winnerProperties['defense'],
            80,
            80
        );
        $loser = $this->playerFactory->createPlayer(
            $loserProperties['health'],
            $loserProperties['strength'],
            $loserProperties['defense'],
            70,
            80
        );

        $gameResult = $this->playGame($winner, $loser);

        static::assertEquals($winnerHealth, $winner->getHealth(), 'Winner health mismatch');
        static::assertEquals($loserHealth, $loser->getHealth(), 'Loser health mismatch');
        static::assertEquals($rounds, $gameResult->getNumberOfRounds());
        static::assertSame(
            $winner,
            $gameResult->getWinnerPlayer(),
            'First player should win'
        );
    }

    /**
     * second player win after four rounds
     */
    public function testSecondPlayerWinAfterFourRounds()
    {
        $loser = $this->playerFactory->createPlayer(80, 70, 50, 80, 80);
        $winner = $this->playerFactory->createPlayer(80, 90, 60, 70, 80);

        $gameResult = $this->playGame($loser, $winner);

        static::assertEquals(0, $loser->getHealth());
        static::assertEquals(60, $winner->getHealth());
        static::assertEquals(4, $gameResult->getNumberOfRounds());
        static::assertSame(
            $winner,
            $gameResult->getWinnerPlayer(),
            'Second player should win'
        );
    }

    /**
     * a defender with luck thirty percent should be affected by only six attacks from twenty
     * @todo review this tests, the code smells here
     */
    public function testADefenderWithLuckThirtyPercentShouldBeAffectedByOnlyFourteenAttacksFromTwenty()
    {
        $this->luckProbability = $this->getMockBuilder(LuckProbability::class)
            ->getMock();
        $this->luckProbability->method('iAmLucky')
            ->willReturnOnConsecutiveCalls(
                true, false, true, false, true, false, true, false, false, false,
                false, false, false, false, false, false, false, false, false, false
            );
        $attacker = $this->playerFactory->createPlayer(100, 100, 100, 100, 100);
        $defender = $this->playerFactory->createPlayer(100, 100, 95, 50, 30);
        $this->playGame($attacker, $defender);

        static::assertEquals(70, $defender->getHealth());
    }

    /**
     * @param $attacker
     * @param $defender
     * @return \HeroGame\GameResult
     * @throws \HeroGame\Exceptions\GameNumberOfPlayers
     */
    private function playGame(Player $attacker, Player $defender): \HeroGame\GameResult
    {
        $game = new Game($this->luckProbability);

        $game->addPlayer($attacker);
        $game->addPlayer($defender);

        $gameResult = $game->play();
        return $gameResult;
    }
}
