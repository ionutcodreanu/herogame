<?php

namespace HeroGame\Tests\Integration\Game;

use HeroGame\Game;
use HeroGame\Game\Round\Result;
use HeroGame\Game\Round\ResultCollection;
use HeroGame\GameResult;
use HeroGame\Player;
use HeroGame\PlayerFactory;
use HeroGame\Tests\Stubs\NeverLucky;
use PHPUnit\Framework\TestCase;

class RoundResultTest extends TestCase
{
    /**
     * after playing a game the game result should contain every round result
     */
    public function testAfterPlayingAGameTheGameResultShouldContainEveryRoundResult()
    {
        list($hero, $beast) = $this->initGamePlayers();
        $game = new Game(new NeverLucky());
        $game->addPlayer($hero);
        $game->addPlayer($beast);

        $gameResult = $game->play();
        static::assertInstanceOf(GameResult::class, $gameResult);

        $resultsPerRound = $gameResult->getRoundResult();
        static::assertInstanceOf(ResultCollection::class, $resultsPerRound);

        static::assertCount(3, $resultsPerRound, 'Number of rounds does not match');
        $resultsPerRound->rewind();
        /** @var Result $firstRound */
        $firstRound = $resultsPerRound->current();

        static::assertInstanceOf(Result::class, $firstRound);
        static::assertEquals(1, $firstRound->getRoundNumber());
        static::assertEquals(
            'Hero',
            $firstRound->getAttacker()->getName(),
            'Attacker name should be the Hero'
        );
        static::assertEquals(
            'Wild Beast',
            $firstRound->getDefender()->getName(),
            'Defender name should be the Wild Beast'
        );
        static::assertEquals(
            10,
            $firstRound->getDefender()->getLastDamage()
        );

        $resultsPerRound->next();
        /** @var Result $secondRoundResult */
        $secondRoundResult = $resultsPerRound->current();

        static::assertInstanceOf(Result::class, $secondRoundResult);
        static::assertEquals(2, $secondRoundResult->getRoundNumber());
        static::assertEquals(
            'Wild Beast',
            $secondRoundResult->getAttacker()->getName(),
            'Attacker name should be the Wild Beast'
        );
        static::assertEquals(
            'Hero',
            $secondRoundResult->getDefender()->getName(),
            'Defender name should be the Hero'
        );
        static::assertEquals(
            5,
            $secondRoundResult->getDefender()->getLastDamage()
        );
    }

    /**
     * @return Player[]
     */
    private function initGamePlayers(): array
    {
        $playerFactory = new PlayerFactory();
        $hero = $playerFactory->createPlayer(100, 100, 95, 100, 100);
        $hero->setName('Hero');
        $beast = $playerFactory->createPlayer(20, 100, 90, 1, 0);
        $beast->setName('Wild Beast');
        return array($hero, $beast);
    }
}
