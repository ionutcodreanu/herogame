<?php

namespace HeroGame\Tests\Integration;

use HeroGame\Player;
use HeroGame\PlayerBuilder;
use HeroGame\PlayerFactory;
use HeroGame\Tests\Stubs\MeanGenerator;
use PHPUnit\Framework\TestCase;

class PlayerBuilderTest extends TestCase
{
    /** @var  PlayerBuilder */
    private $playerBuilder;

    protected function setUp()
    {
        $this->playerBuilder = new PlayerBuilder(
            new MeanGenerator()
        );
    }

    /**
     * i can build a basic player
     */
    public function testICanBuildABasicPlayer()
    {
        $player = $this->playerBuilder->build();
        static::assertInstanceOf(Player::class, $player);
    }

    /**
     * i can build a basic player with health between fifty and sixty with a mean number generator
     */
    public function testICanBuildABasicPlayerWithHealthBetweenFiftyAndSixty()
    {
        $player = $this->playerBuilder
            ->withHealthRange(50, 60)
            ->build();
        static::assertInstanceOf(Player::class, $player);
        static::assertEquals(55, $player->getHealth());
    }

    /**
     * i can build a basic player with health between 25 and 70 with a mean number generator
     */
    public function testICanBuildABasicPlayerWithHealthBetween25And70WithAMeanNumberGenerator()
    {
        $player = $this->playerBuilder
            ->withHealthRange(25, 70)
            ->build();
        static::assertEquals(48, $player->getHealth());
    }

    /**
     * i can build a basic player with strength between 15 and 70 with a mean number generator
     */
    public function testICanBuildABasicPlayerWithStrengthBetween15And70WithAMeanNumberGenerator()
    {
        $player = $this->playerBuilder
            ->withStrengthRange(15, 70)
            ->build();
        static::assertEquals(43, $player->getStrength());
    }

    /**
     * i can build a basic player with defense between 35 and 42 with a mean number generator
     */
    public function testICanBuildABasicPlayerWithDefenseBetween35And42WithAMeanNumberGenerator()
    {
        $player = $this->playerBuilder
            ->withDefenseRange(35, 42)
            ->build();
        static::assertEquals(39, $player->getDefense());
    }

    /**
     * i can build a basic player with speed between 35 and 32 with a mean number generator
     */
    public function testICanBuildABasicPlayerWithSpeedBetween35And32WithAMeanNumberGenerator()
    {
        $player = $this->playerBuilder
            ->withSpeedRange(35, 32)
            ->build();
        static::assertEquals(34, $player->getSpeed());
    }
    /**
     * i can build a basic player with luck between 25 and 32 with a mean number generator
     */
    public function testICanBuildABasicPlayerWithLuckBetween25And32WithAMeanNumberGenerator()
    {
        $player = $this->playerBuilder
            ->withLuckRange(25, 32)
            ->build();
        static::assertEquals(29, $player->getLuck());
    }
}
