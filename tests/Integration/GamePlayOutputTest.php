<?php

namespace HeroGame\Tests\Integration;

use HeroGame\Game;
use HeroGame\Player;
use HeroGame\PlayerFactory;
use HeroGame\Tests\Stubs\ControlledLuck;
use HeroGame\Tests\Stubs\DefensiveSkill;
use HeroGame\Tests\Stubs\NeverLucky;
use HeroGame\Tests\Stubs\OffensiveSkill;
use PHPUnit\Framework\TestCase;

class GamePlayOutputTest extends TestCase
{
    /**
     * after each round the game should display who attack and who defend
     */
    public function testAfterEachRoundTheGameShouldDisplayWhoAttackAndWhoDefend()
    {
        $game = $this->initBasicGame();

        $gameResult = $game->play();
        $outputGenerator = $this->createGameOutput();
        $outputGenerator->generate($gameResult);

        static::expectOutputString(
            "Round 1:\n" .
            "\tHero attacked Wild Beast.\n" .
            "\tWild Beast damage was 10.\n" .
            "\tWild Beast current health is: 10\n" .
            "Round 2:\n" .
            "\tWild Beast attacked Hero.\n" .
            "\tHero damage was 5.\n" .
            "\tHero current health is: 95\n" .
            "Round 3:\n" .
            "\tHero attacked Wild Beast.\n" .
            "\tWild Beast damage was 10.\n" .
            "\tWild Beast current health is: 0\n" .
            "Game Result:\n" .
            "\tHero wins the game.\n"
        );
    }

    /**
     * after a round when the defender was lucky the event should be displayed
     */
    public function testAfterARoundWhenTheDefenderWasLuckyTheEventShouldBeDisplayed()
    {
        list($hero, $beast) = $this->initGamePlayers();
        $game = new Game(new ControlledLuck());

        $game->addPlayer($hero);
        $game->addPlayer($beast);

        $gameResult = $game->play();
        $outputGenerator = $this->createGameOutput();
        $outputGenerator->generate($gameResult);

        static::expectOutputString(
            "Round 1:\n" .
            "\tHero attacked Wild Beast.\n" .
            "\tWild Beast damage was 10.\n" .
            "\tWild Beast current health is: 10\n" .
            "Round 2:\n" .
            "\tWild Beast attacked Hero.\n" .
            "\tHero was lucky and avoided attack.\n" .
            "\tHero current health is: 100\n" .
            "Round 3:\n" .
            "\tHero attacked Wild Beast.\n" .
            "\tWild Beast damage was 10.\n" .
            "\tWild Beast current health is: 0\n" .
            "Game Result:\n" .
            "\tHero wins the game.\n"
        );
    }

    /**
     * when an offensive skill is applied the event should be dispatched
     */
    public function testWhenAnOffensiveSkillIsAppliedTheEventShouldBeDispatched()
    {
        list($hero, $beast) = $this->initGamePlayers();
        $game = new Game(new NeverLucky());
        $skill = new OffensiveSkill();
        $hero->addSkill($skill);
        $game->addPlayer($hero);
        $game->addPlayer($beast);

        $gameResult = $game->play();
        $outputGenerator = $this->createGameOutput();
        $outputGenerator->generate($gameResult);
        static::expectOutputString(
            "Round 1:\n" .
            "\tHero attacked Wild Beast.\n" .
            "\tHero applied Offensive skill.\n" .
            "\tWild Beast damage was 10.\n" .
            "\tWild Beast current health is: 10\n" .
            "Round 2:\n" .
            "\tWild Beast attacked Hero.\n" .
            "\tHero damage was 5.\n" .
            "\tHero current health is: 95\n" .
            "Round 3:\n" .
            "\tHero attacked Wild Beast.\n" .
            "\tHero applied Offensive skill.\n" .
            "\tWild Beast damage was 10.\n" .
            "\tWild Beast current health is: 0\n" .
            "Game Result:\n" .
            "\tHero wins the game.\n"
        );
    }

    /**
     * when an defensive skill occur then the event should be dispatched
     */
    public function testWhenAnDefensiveSkillOccurThenTheEventShouldBeDispatched()
    {
        list($hero, $beast) = $this->initGamePlayers();
        $game = new Game(new NeverLucky());
        $skill = new DefensiveSkill();
        $beast->addSkill($skill);
        $game->addPlayer($hero);
        $game->addPlayer($beast);

        $gameResult = $game->play();
        $outputGenerator = $this->createGameOutput();
        $outputGenerator->generate($gameResult);

        static::expectOutputString(
            "Round 1:\n" .
            "\tHero attacked Wild Beast.\n" .
            "\tWild Beast damage was 10.\n" .
            "\tWild Beast applied Defensive skill.\n" .
            "\tWild Beast current health is: 10\n" .
            "Round 2:\n" .
            "\tWild Beast attacked Hero.\n" .
            "\tHero damage was 5.\n" .
            "\tHero current health is: 95\n" .
            "Round 3:\n" .
            "\tHero attacked Wild Beast.\n" .
            "\tWild Beast damage was 10.\n" .
            "\tWild Beast applied Defensive skill.\n" .
            "\tWild Beast current health is: 0\n" .
            "Game Result:\n" .
            "\tHero wins the game.\n"
        );
    }

    /**
     * when after twenty rounds no player wins then the output should return no winner message
     */
    public function testWhenAfterTwentyRoundsNoPlayerWinsThenTheOutputShouldReturnNoWinnerMessage()
    {
        list($hero, $beast) = $this->initGamePlayers();
        $game = new Game(new NeverLucky());

        $beast->setHealth(100);
        $hero->setStrength(95);
        $game->addPlayer($hero);
        $game->addPlayer($beast);
        $gameResult = $game->play();
        $outputGenerator = $this->createGameOutput();
        $outputGenerator->generate($gameResult);
        static::expectOutputRegex('/.*Game Result:\n.*No game winner\./');
    }

    /**
     * @return Player[]
     */
    private function initGamePlayers(): array
    {
        $playerFactory = new PlayerFactory();
        $hero = $playerFactory->createPlayer(100, 100, 95, 100, 100);
        $hero->setName('Hero');
        $beast = $playerFactory->createPlayer(20, 100, 90, 1, 0);
        $beast->setName('Wild Beast');
        return array($hero, $beast);
    }

    /**
     * @return Game\Output
     */
    private function createGameOutput(): Game\Output
    {
        return new Game\Output(new Game\Console());
    }

    /**
     * @return Game
     * @throws \HeroGame\Exceptions\GameNumberOfPlayers
     */
    private function initBasicGame(): Game
    {
        list($hero, $beast) = $this->initGamePlayers();
        $game = new Game(new NeverLucky());
        $game->addPlayer($hero);
        $game->addPlayer($beast);
        return $game;
    }
}
