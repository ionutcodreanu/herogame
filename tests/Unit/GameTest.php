<?php

namespace HeroGame\Tests\Unit;

use HeroGame\Exceptions\GameNumberOfPlayers;
use HeroGame\Game;
use HeroGame\GameResult;
use HeroGame\PlayerFactory;
use HeroGame\Tests\Stubs\NeverLucky;
use PHPUnit\Framework\TestCase;

class GameTest extends TestCase
{
    /** @var  Game */
    private $game;
    /**
     * @var PlayerFactory
     */
    private $playerFactory;

    protected function setUp()
    {
        $this->playerFactory = new PlayerFactory();
        $this->game = new Game(new NeverLucky());
    }

    /**
     * i should be able to play a game and get the result
     */
    public function testIShouldBeAbleToPlayAGameAndGetTheResult()
    {
        $this->setUpGamePlayers();
        $gameResult = $this->game->play();
        static::assertInstanceOf(GameResult::class, $gameResult);
    }

    /**
     * i can not start a game with no player
     */
    public function testICanNotStartAGameWithNoPlayer()
    {
        static::expectException(GameNumberOfPlayers::class);
        static::expectExceptionMessage('The game can not start without no players');
        $this->game->play();
    }

    /**
     * i can not start a game with only one player
     */
    public function testICanNotStartAGameWithOnlyOnePlayer()
    {
        $attacker = $this->playerFactory->createPlayer(100, 100, 100, 100, 0);
        $this->game->addPlayer($attacker);
        static::expectException(GameNumberOfPlayers::class);
        static::expectExceptionMessage('The game can not start with only one player');
        $this->game->play();
    }

    /**
     * i can not add more than two players to the game
     */
    public function testICanNotAddMoreThanTwoPlayersToTheGame()
    {
        static::expectException(GameNumberOfPlayers::class);
        static::expectExceptionMessage('The maximum number of players is 2');

        $this->setUpGamePlayers();
        $thirdPlayer = $this->playerFactory->createPlayer(100, 100, 100, 100, 0);
        $this->game->addPlayer($thirdPlayer);
    }

    /**
     * a player with greater speed then the other should start the game
     */
    public function testAPlayerWithGreaterSpeedThenTheOtherShouldStartTheGame()
    {
        $attacker = $this->playerFactory->createPlayer(100, 100, 100, 90, 0);
        $defender = $this->playerFactory->createPlayer(100, 100, 100, 100, 0);

        $this->game->addPlayer($attacker);
        $this->game->addPlayer($defender);
        $gameResult = $this->game->play();
        static::assertSame($defender, $gameResult->getFirstRoundAttacker());
    }


    /**
     * a player with a greater luck and with same speed should start the game
     */
    public function testAPlayerWithAGreaterLuckAndWithSameSpeedShouldStartTheGame()
    {
        $attacker = $this->playerFactory->createPlayer(100, 100, 100, 100, 50);
        $defender = $this->playerFactory->createPlayer(100, 100, 100, 100, 0);

        $this->game->addPlayer($attacker);
        $this->game->addPlayer($defender);
        $gameResult = $this->game->play();
        static::assertSame(
            $attacker,
            $gameResult->getFirstRoundAttacker(),
            'The player with greater luck should start the game'
        );
    }

    /**
     * first player have speed greater than the second player and should start the game
     */
    public function testSecondPlayerHaveSpeedGreaterThanFirstPlayerAndShouldStartTheGame()
    {
        $attacker = $this->playerFactory->createPlayer(100, 100, 100, 90, 0);
        $defender = $this->playerFactory->createPlayer(100, 100, 100, 100, 0);

        $this->game->addPlayer($defender);
        $this->game->addPlayer($attacker);
        $gameResult = $this->game->play();
        static::assertSame($defender, $gameResult->getFirstRoundAttacker());
    }

    /**
     * who should start the game if two players have the same speed and luck
     * @todo check requirements
     */
    public function testWhoShouldStartTheGameIfTwoPlayersHaveTheSameSpeedAndLuck()
    {
        $attacker = $this->playerFactory->createPlayer(100, 100, 100, 100, 10);
        $defender = $this->playerFactory->createPlayer(100, 100, 100, 100, 10);

        $this->game->addPlayer($defender);
        $this->game->addPlayer($attacker);
        $gameResult = $this->game->play();
        static::assertSame(
            $attacker,
            $gameResult->getFirstRoundAttacker(),
            'If two players have the same speed and luck the second player should start the game'
        );
    }

    private function setUpGamePlayers(): void
    {
        $attacker = $this->playerFactory->createPlayer(100, 100, 100, 100, 0);
        $defender = $this->playerFactory->createPlayer(100, 100, 100, 100, 0);

        $this->game->addPlayer($attacker);
        $this->game->addPlayer($defender);
    }
}
