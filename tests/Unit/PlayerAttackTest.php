<?php

namespace HeroGame\Tests\Unit;

use HeroGame\PlayerFactory;
use PHPUnit\Framework\TestCase;

class PlayerAttackTest extends TestCase
{
    /**
     * @var PlayerFactory
     */
    private $playerFactory;

    protected function setUp()
    {
        $this->playerFactory = new PlayerFactory();
    }

    /**
     * after one player attack the other player then the damage should be visible for the second player
     * @dataProvider secondPlayerDefender
     * @param $secondPlayerCharacteristics
     * @param int $expectedHealth
     */
    public function testGivenIHaveTwoPlayersIShouldAttackEachOther($secondPlayerCharacteristics, int $expectedHealth)
    {
        $attacker = $this->playerFactory->createPlayer(50, 50, 50, 50, 50);
        $defender = $this->playerFactory->createPlayer(
            $secondPlayerCharacteristics['health'],
            $secondPlayerCharacteristics['strength'],
            $secondPlayerCharacteristics['defense'],
            50,
            50
        );

        $attacker->attack($defender);
        static::assertEquals($expectedHealth, $defender->getHealth());
        static::assertEquals(50, $attacker->getHealth());
    }

    /**
     * after one player attack a player with greater defense then the damage is for first player
     */
    public function testAfterOnePlayerAttackAPlayerWithGreaterDefenseThenTheDamageIsForFirstPlayer()
    {
        $attacker = $this->playerFactory->createPlayer(35, 35, 50, 50, 50);
        $defender = $this->playerFactory->createPlayer(50, 50, 50, 50, 50);

        $attacker->attack($defender);
        static::assertEquals(20, $attacker->getHealth());
        static::assertEquals(50, $defender->getHealth());
    }

    /**
     * one player attacking other player that have defense equals with first player strength should not affect health
     */
    public function testOnePlayerAttackingOtherPlayerThatHaveDefenseEqualsWithFirstPlayerStrengthShouldNotAffectHealth()
    {
        $attacker = $this->playerFactory->createPlayer(35, 35, 35, 50, 50);
        $defender = $this->playerFactory->createPlayer(35, 35, 35, 50, 50);

        $attacker->attack($defender);
        static::assertEquals(35, $attacker->getHealth());
        static::assertEquals(35, $defender->getHealth());
    }

    public function secondPlayerDefender()
    {
        return [
            [
                [
                    'health' => 45,
                    'strength' => 45,
                    'defense' => 45,
                ],
                40,
            ],
            [
                [
                    'health' => 49,
                    'strength' => 49,
                    'defense' => 49,
                ],
                48,
            ],
            [
                [
                    'health' => 50,
                    'strength' => 50,
                    'defense' => 50,
                ],
                50,
            ]
        ];
    }
}
