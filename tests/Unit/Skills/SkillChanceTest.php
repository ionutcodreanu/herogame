<?php

namespace HeroGame\Tests\Unit\Skills;

use HeroGame\Player;
use HeroGame\Skills\Chance;
use HeroGame\Tests\Stubs\ChanceGenerator;
use HeroGame\Tests\Stubs\Skill;
use PHPUnit\Framework\TestCase;

class SkillChanceTest extends TestCase
{
    /**
     * given a skill with chance of 100 percent to be applied it should be applied always
     */
    public function testGivenASkillWithChanceOf100PercentToBeAppliedItShouldBeAppliedAlways()
    {
        $skill = new Skill();
        $skill->withChance(new ChanceGenerator(100));
        $attacker = new Player();
        $defender = new Player();
        $noOfAttacks = 10;
        for ($attack = 1; $attack <= $noOfAttacks; $attack++) {
            $skill->apply($attacker, $defender);
        }
        static::assertEquals($skill->noOfOccurrences, $noOfAttacks);
    }


    /**
     * given a skill with chance of 0 percent then the skill should no reveal
     */
    public function testGivenASkillWithChanceOf0PercentThenTheSkillShouldNoReveal()
    {
        $skill = new Skill();
        $skill->withChance(new ChanceGenerator(0));
        $attacker = new Player();
        $defender = new Player();
        $noOfAttacks = 10;
        for ($attack = 1; $attack <= $noOfAttacks; $attack++) {
            $skill->apply($attacker, $defender);
        }
        static::assertEquals(0, $skill->noOfOccurrences);
    }


    /**
     * given a skill with change of 50 percent should reveal five times from ten
     */
    public function testGivenASkillWithChangeOf50PercentShouldRevealFiveTimesFromTen()
    {
        $skill = new Skill();

        $chance = $this->getMockBuilder(Chance::class)->getMock();
        $chance->method('iHaveChance')
            ->willReturnOnConsecutiveCalls(
                true, true, true, true, true,
                false, false, false, false, false
            );
        $skill->withChance($chance);
        $attacker = new Player();
        $defender = new Player();
        $noOfAttacks = 10;
        for ($attack = 1; $attack <= $noOfAttacks; $attack++) {
            $skill->apply($attacker, $defender);
        }
        static::assertEquals($skill->noOfOccurrences, 5);
    }
}
