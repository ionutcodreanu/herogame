<?php

namespace HeroGame\Tests\Unit\Skills;

use HeroGame\PlayerFactory;
use HeroGame\Skills\MagicShield;
use HeroGame\Skills\Skill;
use PHPUnit\Framework\TestCase;

class MagicShieldSkillTest extends TestCase
{
    /** @var  Skill */
    private $skill;
    /** @var  PlayerFactory */
    private $playerFactory;

    protected function setUp()
    {
        $this->skill = new MagicShield();
        $this->playerFactory = new PlayerFactory();

        static::assertInstanceOf(Skill::class, $this->skill);
        static::assertEquals(Skill::DEFENSIVE, $this->skill->getType());
    }

    /**
     * given the magic shield defensive skill the defender damage should be half
     */
    public function testGivenTheMagicShieldDefensiveSkillTheDefenderDamageShouldBeHalf()
    {
        list($attacker, $defender) = $this->createPlayers();

        $this->skill->apply($attacker, $defender);
        static::assertEquals(100, $attacker->getHealth());
        static::assertEquals(54, $defender->getHealth());
    }

    /**
     * given a defender with magic shield after attack the damage should be only half
     */
    public function testGivenADefenderWithMagicShieldAfterAttackTheDamageShouldBeOnlyHalf()
    {
        list($attacker, $defender) = $this->createPlayers();

        $defender->addSkill($this->skill);
        $attacker->attack($defender);
        static::assertEquals(100, $attacker->getHealth());
        static::assertEquals(45, $defender->getHealth());
    }

    /**
     * @return array
     */
    private function createPlayers(): array
    {
        $attacker = $this->playerFactory->createPlayer(100, 100, 100, 100, 100);
        $defender = $this->playerFactory->createPlayer(50, 100, 91, 100, 0);
        return array($attacker, $defender);
    }
}
