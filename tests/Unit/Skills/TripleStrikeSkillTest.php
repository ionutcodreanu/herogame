<?php

namespace HeroGame\Tests\Unit\Skills;

use HeroGame\PlayerFactory;
use HeroGame\Skills\Skill;
use HeroGame\Skills\TripleStrike;
use PHPUnit\Framework\TestCase;

class TripleStrikeSkillTest extends TestCase
{
    /** @var  Skill */
    private $skill;
    /** @var  PlayerFactory */
    private $playerFactory;

    protected function setUp()
    {
        $this->skill = new TripleStrike();
        $this->playerFactory = new PlayerFactory();

        static::assertInstanceOf(Skill::class, $this->skill);
        static::assertEquals(Skill::OFFENSIVE, $this->skill->getType());
    }

    /**
     * given a triple strike skill the attacker should attack twice
     */
    public function testGivenATripleStrikeSkillTheAttackerShouldAttackTwice()
    {
        list($attacker, $defender) = $this->createPlayers();

        $this->skill->apply($attacker, $defender);

        static::assertEquals(100, $attacker->getHealth());
        static::assertEquals(80, $defender->getHealth());
    }

    /**
     * given a triple strike skill attached to an attacker the attacker should attack three times
     */
    public function testGivenATripleStrikeSkillAttachedToAnAttackerTheAttackerShouldAttackThreeTimes()
    {
        list($attacker, $defender) = $this->createPlayers();

        $attacker->addSkill($this->skill);
        $attacker->attack($defender);
        static::assertEquals(100, $attacker->getHealth());
        static::assertEquals(70, $defender->getHealth());
    }

    /**
     * @return array
     */
    private function createPlayers(): array
    {
        $attacker = $this->playerFactory->createPlayer(100, 100, 100, 100, 100);
        $defender = $this->playerFactory->createPlayer(100, 90, 90, 90, 90);
        return array($attacker, $defender);
    }
}
