<?php

namespace HeroGame\Tests\Unit\Skills;

use HeroGame\PlayerFactory;
use HeroGame\Skills\DoubleStrike;
use HeroGame\Skills\Skill;
use PHPUnit\Framework\TestCase;

class DoubleStrikeSkillTest extends TestCase
{
    /** @var  DoubleStrike */
    private $skill;
    /** @var  PlayerFactory */
    private $playerFactory;

    protected function setUp()
    {
        $this->skill = new DoubleStrike();
        $this->playerFactory = new PlayerFactory();

        static::assertInstanceOf(Skill::class, $this->skill);
        static::assertEquals(Skill::OFFENSIVE, $this->skill->getType());
    }

    /**
     * given a double strike skill applied the attacker should attack again
     */
    public function testGivenADoubleStrikeSkillAppliedTheAttackerShouldAttackAgain()
    {
        list($attacker, $defender) = $this->createPlayers();
        $this->skill->apply($attacker, $defender);

        static::assertEquals(95, $defender->getHealth(), 'Defender health');
        static::assertEquals(100, $attacker->getHealth(), 'Attacker health');
    }

    /**
     * given a double strike skill attached to a player the attacker should attack twice
     */
    public function testGivenADoubleStrikeSkillAttachedToAPlayerTheAttackerShouldAttackTwice()
    {
        list($attacker, $defender) = $this->createPlayers();

        $attacker->addSkill($this->skill);
        $attacker->attack($defender);

        static::assertEquals(90, $defender->getHealth(), 'Defender health');
        static::assertEquals(100, $attacker->getHealth(), 'Attacker health');
    }

    /**
     * @return array
     */
    private function createPlayers(): array
    {
        $attacker = $this->playerFactory->createPlayer(100, 100, 100, 100, 50);
        $defender = $this->playerFactory->createPlayer(100, 100, 95, 100, 50);
        return array($attacker, $defender);
    }
}
