<?php

namespace HeroGame\Tests\Unit\Skills;

use HeroGame\Skills\DoubleStrike;
use HeroGame\Skills\MagicShield;
use HeroGame\Skills\TripleStrike;
use HeroGame\Tests\Stubs\DefensiveSkill;
use HeroGame\Tests\Stubs\OffensiveSkill;
use HeroGame\Tests\Stubs\Skill;
use PHPUnit\Framework\TestCase;

class SkillTest extends TestCase
{
    /**
     * i can create a defensive skill
     */
    public function testICanCreateADefensiveSkill()
    {
        $skill = new DefensiveSkill();
        static::assertFalse($skill->isOffensive(), 'Skill should be defensive');
    }

    /**
     * i can create an offensive skill
     */
    public function testICanCreateAnOffensiveSkill()
    {
        $skill = new OffensiveSkill();
        static::assertTrue($skill->isOffensive(), 'Skill should be offensive');
    }

    /**
     * the skill should have a name
     */
    public function testTheSkillShouldHaveAName()
    {
        $skill = new OffensiveSkill();
        static::assertEquals('Offensive', $skill->getName());

        $doubleStrike = new DoubleStrike();
        static::assertEquals('Double Strike', $doubleStrike->getName());

        $tripleStrike = new TripleStrike();
        static::assertEquals('Triple Strike', $tripleStrike->getName());

        $magicShield = new MagicShield();
        static::assertEquals('Magic Shield', $magicShield->getName());
    }
}
