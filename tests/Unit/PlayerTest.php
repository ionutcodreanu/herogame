<?php

namespace HeroGame\Tests\Unit;

use HeroGame\Exceptions\PlayerInvalidLuck;
use HeroGame\Exceptions\PlayerInvalidDefense;
use HeroGame\Exceptions\PlayerInvalidHealth;
use HeroGame\Exceptions\PlayerInvalidSpeed;
use HeroGame\Exceptions\PlayerInvalidStrength;
use HeroGame\Player;
use HeroGame\PlayerFactory;
use PHPUnit\Framework\TestCase;

class PlayerTest extends TestCase
{
    /**
     * @var PlayerFactory
     */
    private $playerFactory;

    protected function setUp()
    {
        $this->playerFactory = new PlayerFactory();
    }

    /**
     * the player health should be between 0 and 100
     * @dataProvider playerCharacteristicsTestCases
     * @param int $health
     * @param bool $valid
     * @throws PlayerInvalidHealth
     */
    public function testThePlayerHealthShouldBeBetween0And100(int $health, bool $valid)
    {
        if (false === $valid) {
            static::expectException(PlayerInvalidHealth::class);
            static::expectExceptionMessage('Player health should be between 0 and 100');
        }

        $player = $this->playerFactory->createPlayer($health, 50, 50, 50, 50);

        if ($valid) {
            static::assertEquals($health, $player->getHealth());
        }
    }

    /**
     * the player strength should be between 0 and 100
     * @dataProvider playerCharacteristicsTestCases
     * @param int $strength
     * @param bool $valid
     */
    public function testThePlayerStrengthShouldBeBetween0And100(int $strength, bool $valid)
    {
        if (false === $valid) {
            static::expectException(PlayerInvalidStrength::class);
            static::expectExceptionMessage('Player strength should be between 0 and 100');
        }
        $player = $this->playerFactory->createPlayer(50, $strength, 50, 50, 50);

        if ($valid) {
            static::assertEquals($strength, $player->getStrength());
        }
    }

    /**
     * the player defense should be between 0 and 100
     * @dataProvider playerCharacteristicsTestCases
     * @param int $defense
     * @param bool $valid
     */
    public function testThePlayerDefenseShouldBeBetween0And100(int $defense, bool $valid)
    {
        if (false === $valid) {
            static::expectException(PlayerInvalidDefense::class);
            static::expectExceptionMessage('Player defense should be between 0 and 100');
        }
        $player = $this->playerFactory->createPlayer(50, 50, $defense, 50, 50);

        if ($valid) {
            static::assertEquals($defense, $player->getDefense());
        }
    }

    /**
     * the player speed should be between 0 and 100
     * @dataProvider playerCharacteristicsTestCases
     * @param int $speed
     * @param bool $valid
     */
    public function testThePlayerSpeedShouldBeBetween0And100(int $speed, bool $valid)
    {
        if (false === $valid) {
            static::expectException(PlayerInvalidSpeed::class);
            static::expectExceptionMessage('Player speed should be between 0 and 100');
        }
        $player = $this->playerFactory->createPlayer(50, 50, 50, $speed, 50);
        if ($valid) {
            static::assertEquals($speed, $player->getSpeed());
        }
    }

    /**
     * the player luck should be between 0 and 100
     * @dataProvider playerLuckTestCases
     * @param int $luck
     * @param bool $valid
     */
    public function testThePlayerLuckShouldBeBetween0And100(int $luck, bool $valid)
    {
        if (false === $valid) {
            static::expectException(PlayerInvalidLuck::class);
            static::expectExceptionMessage('Player luck should be between 0 and 100');
        }
        $player = $this->playerFactory->createPlayer(50, 50, 50, 50, $luck);

        if ($valid) {
            static::assertEquals($luck, $player->getLuck());
        }
    }

    /**
     * the player should have a name
     */
    public function testThePlayerShouldHaveAName()
    {
        $player = new Player();
        $player->setName('Orderus');
        static::assertEquals('Orderus', $player->getName());
    }


    public function playerCharacteristicsTestCases()
    {
        return [
            'Player characteristic less than 0' => [-1, false],
            'Player characteristic equals with 0' => [0, false],
            'Player characteristic grather than 100' => [101, false],
            'Valid characteristic 1' => [1, true],
            'Valid characteristic 100' => [100, true],
            'Valid characteristic 99' => [99, true],
            'Valid characteristic 50' => [50, true],
        ];
    }

    public function playerLuckTestCases()
    {
        return [
            'Player luck less than 0' => [-1, false],
            'Player luck equals with 0' => [0, true],
            'Player luck grather than 100' => [101, false],
            'Valid luck 1' => [1, true],
            'Valid luck 100' => [100, true],
            'Valid luck 99' => [99, true],
            'Valid luck 50' => [50, true],
        ];
    }
}
