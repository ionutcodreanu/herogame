<?php

require_once __DIR__ . './../vendor/autoload.php';

$gameOutput = new \HeroGame\Game\Output(new \HeroGame\Game\Console());
$rangeNumberGenerator = new \HeroGame\RandomRangeGenerator();
$gamePlay = new \HeroGame\Application\GamePlay($gameOutput, $rangeNumberGenerator);
$gamePlay->play();
